import React, { Component } from "react";
import {
    Button,
    View,
} from "react-native";
import { SafeAreaView } from "react-navigation";

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Welcome',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <SafeAreaView>
                <View>
                    <Button
                        title="Go the Second Screen"
                        onPress={() => navigate('Second')}
                    />
                </View>
                <View>
                    <Button
                        title="Go the Third Screen"
                        onPress={() => navigate('Third')}
                    />

                </View>
                <View>
                    <Button
                        title="Go the Fourth Screen"
                        onPress={() => navigate('Fourth')}
                    />

                </View>
            </SafeAreaView>
        );
    }
}