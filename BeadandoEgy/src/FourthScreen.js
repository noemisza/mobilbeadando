import React, { Component } from "react";
import { Text, Button, View, Image, TextInput, StyleSheet } from "react-native";
import { createAppContainer, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { memberExpression } from "@babel/types";
import { ScrollView } from "react-native-gesture-handler";

import DataInput from "./DataInput";
import DataList from "./DataList";
import PlaceInput from "./DataInput";

export default class FourthScreen extends Component {
    static navigationOptions = {
        title: 'Trap',
        headerLeft: null
    };
    state = {
        userInput: []
    };
    inputAddedHandler = userValue => {
        this.setState(prevState => {
            return {
                userInput: prevState.userInput.concat({
                    key: Math.random(),
                    value: userValue
                })
            };
        });
    };

    render() {
        const { navigate } = this.props.navigation;

        return (
            <SafeAreaView>
                <ScrollView>
                    <View style={{ marginLeft: 50 }}>
                         <Text>
                            You are stucked in this page.

                        </Text>
                        <Text>
                            Type password in this nice text input box!

                        </Text>
                        {/*
                        <TextInput
                            style={{ height: 40, width: 200, borderColor: 'blue', borderWidth: 1, marginLeft: 50, }}
                            placeholder="password"
                        /> */}
                        <DataInput onInputAdded={this.inputAddedHandler} />
                        <Text>
                            List of tries:
        </Text>
                        <DataList
                            userInput={this.state.userInput}
                            // onItemDeleted={this.inputDeletedHandler}
                        />
                        

                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 26,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "flex-start"
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        paddingBottom: 10,
    }
});

