import React, { Component } from "react";
import { View, TextInput, Button, StyleSheet } from "react-native"; 
import { NavigationEvents, SafeAreaView } from "react-navigation";

class PlaceInput extends Component {
  state = {
    input: ""
  };
  
  inputChangedHandler = val => {
      this.setState({
          input: val
        });
    };
    // goHome () {
    //     const { navigate } = this.props.navigation;
    //     ()=>navigate('Home');

    // }
    inputSubmitHandler = () => {
        // const { navigate } = this.props.navigation;

        if (this.state.input.trim() === "") {
            return;
        }
        if (this.state.input.trim() === "password") {
            // this.goHome();

        }
        this.textInput.clear();
        this.props.onInputAdded(this.state.input);
        this.state.input= "";
    };
    
    render() {
    return (
        <SafeAreaView>

      <View style={styles.inputContainer}>
        <TextInput
        ref={input => { this.textInput = input }}
          placeholder="password"
          value={this.state.input}
          onChangeText={this.inputChangedHandler}
          style={styles.input}
        />
        <Button
          title="Try"
          style={styles.button}
          onPress={this.inputSubmitHandler}
        />
      </View>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  input: {
      borderColor: 'black',
      borderWidth: 1,
      borderRadius: 10,
    width: "70%"
  },
  button: {
    width: "30%",
  }
});

export default PlaceInput;