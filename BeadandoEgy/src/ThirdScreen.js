import React, { Component } from "react";
import { Text, Button, View, Image } from "react-native";
import { createAppContainer, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { memberExpression } from "@babel/types";


export default class ThirdScreen extends Component {
    static navigationOptions = {
        title: 'Nice picture',
      };
      render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView>
            <View>
                <Image 
                style={{width: 400, height: 400, resizeMode: 'contain'}}
                source={require('./meme.png')}/>
            </View>
          </SafeAreaView>
        );
      }
  
  }

