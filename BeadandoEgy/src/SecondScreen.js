import React, { Component } from "react";
import { Text, Button, View } from "react-native";
import { createAppContainer, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


export default class SecondScreen extends Component {
    static navigationOptions = {
        title: 'Useless Button',
      };
      render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView>
            <View>
            <Button title="Useless button" />
            </View>
          </SafeAreaView>
        );
      }
  
  }

