import React, { Component } from 'react';
import { createAppContainer, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Text,
Button, View } from 'react-native';

import HomeScreen from './src/HomeScreen';
import SecondScreen from './src/SecondScreen';
import ThirdScreen from './src/ThirdScreen';
import FourthScreen from './src/FourthScreen';
import PlaceInput from './src/DataInput';

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Second: {screen: SecondScreen},
  Third: {screen: ThirdScreen},
  Fourth: {screen: FourthScreen},
  data: {screen: PlaceInput}
});

export default App = createAppContainer(MainNavigator);